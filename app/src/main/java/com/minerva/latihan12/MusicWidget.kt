package com.minerva.latihan12

import android.appwidget.AppWidgetManager
import android.appwidget.AppWidgetProvider
import android.content.Context
import android.content.Intent
import android.widget.RemoteViews

/**
 * Implementation of App Widget functionality.
 */
class MusicWidget : AppWidgetProvider() {

    override fun onUpdate(context: Context, appWidgetManager: AppWidgetManager, appWidgetIds: IntArray) {
        // There may be multiple widgets active, so update all of them
        for (appWidgetId in appWidgetIds) {
            updateAppWidget(context, appWidgetManager, appWidgetId)
        }
    }
    override fun onDisabled(context: Context) {
        // Enter relevant functionality for when the last widget is disabled
    }

    override fun onEnabled(context: Context) {
        // Enter relevant functionality for when the first widget is created
    }

    override fun onReceive(context: Context?, intent: Intent?) {
        super.onReceive(context, intent)
        if(intent != null && context != null){
            if(intent.action == ACTION_PLAY){
                Intent(context,MyMPService::class.java).apply {
                    action = ACTION_PLAY
                    context.startService(this)
                }
            }
            if(intent.action == ACTION_SKIPNEXT){
                Intent(context,MyMPService::class.java).apply {
                    action = ACTION_SKIPNEXT
                    context.startService(this)
                }
            }
            if(intent.action == ACTION_SKIPPREV){
                Intent(context,MyMPService::class.java).apply {
                    action = ACTION_SKIPPREV
                    context.startService(this)
                }
            }
        }


    }

    companion object {

        internal fun updateAppWidget(
            context: Context, appWidgetManager: AppWidgetManager,
            appWidgetId: Int
        ) {
            // Construct the RemoteViews object
            val views = RemoteViews(context.packageName, R.layout.music_widget)
            views.setTextViewText(R.id.title_music, "Title : ")
            views.setImageViewResource(R.id.skip_prev, R.drawable.ic_skip_previous_black_24dp)
            views.setImageViewResource(R.id.play, R.drawable.ic_play_arrow_black_24dp)
            views.setImageViewResource(R.id.skip_next, R.drawable.ic_skip_next_black_24dp)

            // Instruct the widget manager to update the widget
            appWidgetManager.updateAppWidget(appWidgetId, views)
        }
    }
}

