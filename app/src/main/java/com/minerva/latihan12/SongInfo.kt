package com.minerva.latihan12

class SongInfo(
    var songName: String?,
    var artistName: String?,
    var songUrl: String?
)