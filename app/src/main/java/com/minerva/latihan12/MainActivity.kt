package com.minerva.latihan12

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.media.MediaPlayer
import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.provider.MediaStore
import android.support.annotation.RequiresApi
import android.support.v4.app.ActivityCompat
import android.widget.SeekBar
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class MainActivity : AppCompatActivity() {

//    private val songList = ArrayList<SongInfo>()
//    private var currentSong = 0
//
//    private var mediaPlayer: MediaPlayer? = null
//
//
//    private val handler = Handler()
//
//    //    var myIntent: Intent? = null
//    private var isPlaying = false
//    private var speed = 0

    val mpService = MyMPService()
    var isPlaying = mpService.isPlaying
    var mediaPlayer = mpService.mediaPlayer
    val songList = mpService.songList
    var currentSong = mpService.currentSong
    var speed = mpService.speed
    val handler = mpService.handler

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        play_media.setOnClickListener {
            if (!isPlaying) {
                if (mediaPlayer != null) {
                    isPlaying = true
                    mediaPlayer?.start()
                } else {
                    val runnable = Runnable {
                        try {
                            mediaPlayer = MediaPlayer()
                            mediaPlayer?.setDataSource(songList[currentSong].songUrl)
                            mediaPlayer?.prepareAsync()
                            mediaPlayer?.setOnPreparedListener { mp ->
                                mp.start()
                                seekBar.progress = 0
                                seekBar.max = mediaPlayer!!.duration
                            }
                            mediaPlayer!!.setOnCompletionListener {
                                stop_media.performClick()
                            }
                            isPlaying = true
                            play_media.setImageResource(R.drawable.ic_pause_black_24dp)
                        } catch (e: Exception) {
                            e.printStackTrace()
                        }
                    }
                    handler.postDelayed(runnable, 1000)
                }
            } else {
                mediaPlayer?.pause()
                isPlaying = false
                play_media.setImageResource(R.drawable.ic_play_arrow_black_24dp)
            }
        }

        stop_media.setOnClickListener {
            if (mediaPlayer != null) {
                isPlaying = false
                play_media.setImageResource(R.drawable.ic_play_arrow_black_24dp)
                mediaPlayer?.stop()
                mediaPlayer?.reset()
                mediaPlayer?.release()
                mediaPlayer = null
                seekBar.progress = 0
                durasiText.text = "00:00"
            }
        }

        forward_media.setOnClickListener {
            if (mediaPlayer != null) {
                if (speed <= 0) {
                    speed += 1000
                }
            }
        }

        rewind_media.setOnClickListener {
            if (mediaPlayer != null) {
                if (speed == 0) {
                    speed -= 2000
                }

                if (speed > 0) {
                    speed -= 1000
                }
            }
        }

        next_media.setOnClickListener {
            stop_media.performClick()
            if (currentSong != songList.lastIndex) {
                currentSong++
            } else {
                currentSong = 0
            }
            judulText.text = "${songList[currentSong].artistName} - ${songList[currentSong].songName}"
        }

        prev_media.setOnClickListener {
            stop_media.performClick()
            if (currentSong != 0) {
                currentSong--
            } else {
                currentSong = songList.lastIndex
            }
            judulText.text = "${songList[currentSong].artistName} - ${songList[currentSong].songName}"
        }

        seekBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                if (mediaPlayer != null && fromUser) {
                    mediaPlayer!!.seekTo(progress)
                }
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {
            }

            override fun onStopTrackingTouch(seekBar: SeekBar?) {

            }
        })

        checkUserPermission()

        val thread = RunThread()
        thread.start()
    }

    inner class RunThread : Thread() {
        override fun run() {
            while (true) {
                try {
                    Thread.sleep(1000)
                } catch (e: InterruptedException) {
                    e.printStackTrace()
                }

                if (isPlaying) {
                    if (speed != 0) {
                        mediaPlayer?.seekTo(mediaPlayer!!.currentPosition + speed)
                    }
                    seekBar.post {
                        seekBar.progress = mediaPlayer!!.currentPosition
                    }
                    durasiText.post {
                        durasiText.text = getTime(mediaPlayer!!.currentPosition)
                    }
                }
            }
        }
    }

    private fun checkUserPermission() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {
                requestPermissions(arrayOf(Manifest.permission.READ_EXTERNAL_STORAGE), 123)
                return
            }
        }
        loadSongs()
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        if (requestCode == 123 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            loadSongs()
        } else {
            Toast.makeText(this, "Permission denied.", Toast.LENGTH_SHORT).show()
            checkUserPermission()
        }
    }

    private fun loadSongs() {
        val uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI
        val selection = MediaStore.Audio.Media.IS_MUSIC + "!=0"
        val cursor = contentResolver.query(uri, null, selection, null, null)
        if (cursor != null) {
            if (cursor.moveToFirst()) {
                do {
                    val name = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.DISPLAY_NAME))
                    val artist = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.ARTIST))
                    val url = cursor.getString(cursor.getColumnIndex(MediaStore.Audio.Media.DATA))

                    val songInfo = SongInfo(name, artist, url)
                    songList.add(songInfo)
                } while (cursor.moveToNext())
            }
            cursor.close()
        }

        judulText.text = "${songList[currentSong].artistName} - ${songList[currentSong].songName}"
    }

    private fun getTime(time: Int): String {
        val timeFormat = SimpleDateFormat("mm:ss", Locale.US)
        return timeFormat.format(time)
    }

    override fun onDestroy() {
        super.onDestroy()
//        stopService(myIntent)
    }
}
