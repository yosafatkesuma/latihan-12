package com.minerva.latihan12

import android.app.Service
import android.content.Intent
import android.media.MediaPlayer
import android.os.Handler
import android.os.IBinder
import android.widget.Toast

const val ACTION_PLAY = "PLAY"
const val ACTION_STOP = "STOP"
const val ACTION_CREATE = "CREATE"
const val ACTION_SKIPNEXT = "NEXT"
const val ACTION_SKIPPREV = "PREVIOUS"

class MyMPService : Service(), MediaPlayer.OnPreparedListener, MediaPlayer.OnErrorListener, MediaPlayer.OnCompletionListener {

    val songList = ArrayList<SongInfo>()
    var currentSong = 0

    var mediaPlayer: MediaPlayer? = null
   val handler = Handler()

    //    var myIntent: Intent? = null
    var isPlaying = false
    var speed = 0

    private var firstInit = true

    fun init() {
        mediaPlayer = MediaPlayer.create(this, R.raw.aladdin)
        firstInit = true
        mediaPlayer!!.setOnPreparedListener(this)
        mediaPlayer!!.setOnCompletionListener(this)
    }

    override fun onBind(intent: Intent): IBinder? {
        return null
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        if (intent != null) {
            var actionIntent = intent.action
            when (actionIntent) {
                ACTION_CREATE -> init()
                ACTION_PLAY -> {
                    if (!mediaPlayer!!.isPlaying) {
                        firstInit = false
                        mediaPlayer!!.prepareAsync()
                        if (mediaPlayer != null) {
                            isPlaying = true
                            mediaPlayer!!.start()
                        }
                        else{
                            isPlaying = false
                            mediaPlayer!!.pause()
                        }
                    }
                    else{
                        firstInit = true
                        mediaPlayer!!.prepareAsync()
                        mediaPlayer!!.pause()
                    }
                }
                ACTION_STOP -> {
                    mediaPlayer!!.stop()
                    mediaPlayer!!.release()
                }
                ACTION_SKIPNEXT ->skipNext()
                ACTION_SKIPPREV -> skipPrev()
            }
        }

        return flags
    }

    fun skipNext(){
        mediaPlayer!!.stop()
        mediaPlayer!!.reset()
        mediaPlayer!!.release()
        mediaPlayer = null
        currentSong += 1
        if(currentSong > songList.count()-1){
            currentSong = 0
        }
    }

    fun skipPrev(){
        mediaPlayer!!.stop()
        mediaPlayer!!.reset()
        mediaPlayer!!.release()
        mediaPlayer = null
        currentSong += 1
        if(currentSong == 0){
            currentSong = songList.count()-1
        }
    }

    override fun onError(mp: MediaPlayer?, what: Int, extra: Int): Boolean {
        return false
    }

    override fun onCompletion(mp: MediaPlayer?) {
        Toast.makeText(this, "Player Stop", Toast.LENGTH_SHORT).show()
    }

    override fun onPrepared(mp: MediaPlayer?) {
        mediaPlayer!!.start()
        if (firstInit) {
            mediaPlayer!!.stop()
        }
    }

    override fun onDestroy() {
        if (mediaPlayer != null) {
            mediaPlayer!!.release()
        }
    }
}
